"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("categories", [
      {
        id: 1,
        name: "Voće",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 2,
        name: "Koštunjavo voće",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 3,
        name: "Povrće",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 4,
        name: "Meso",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 5,
        name: "Jaja",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 6,
        name: "Mlečni proizvodi",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 7,
        name: "Žitarice",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 8,
        name: "Mahunarke",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 9,
        name: "Riba",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 10,
        name: "Masnoće",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 11,
        name: "Slatkiši",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 12,
        name: "Pića",
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ]);
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("categories", null, {});
  }
};
