"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("products", [
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Ananas",
        kcal: "56",
        carbs: "13",
        proteins: "0",
        fat: "0",
        categoryId: 1
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Banane",
        kcal: "99",
        carbs: "23",
        proteins: "1",
        fat: "0",
        categoryId: 1
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Borovnice",
        kcal: "62",
        carbs: "14",
        proteins: "1",
        fat: "1",
        categoryId: 1
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Breskve",
        kcal: "46",
        carbs: "11",
        proteins: "1",
        fat: "0",
        categoryId: 1
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Dinje",
        kcal: "24",
        carbs: "5",
        proteins: "1",
        fat: "0",
        categoryId: 1
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Grožđe",
        kcal: "70",
        carbs: "16",
        proteins: "1",
        fat: "0",
        categoryId: 1
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Grejpfrut",
        kcal: "42",
        carbs: "10",
        proteins: "1",
        fat: "0",
        categoryId: 1
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Jabuka",
        kcal: "52",
        carbs: "12",
        proteins: "0",
        fat: "0",
        categoryId: 1
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Jagode",
        kcal: "36",
        carbs: "7",
        proteins: "1",
        fat: "0",
        categoryId: 1
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Kivi",
        kcal: "55",
        carbs: "11",
        proteins: "1",
        fat: "1",
        categoryId: 1
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Kruške",
        kcal: "55",
        carbs: "12",
        proteins: "0",
        fat: "0",
        categoryId: 1
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Lubenica",
        kcal: "24",
        carbs: "5",
        proteins: "1",
        fat: "0",
        categoryId: 1
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Maline",
        kcal: "40",
        carbs: "8",
        proteins: "1",
        fat: "0",
        categoryId: 1
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Mandarine",
        kcal: "48",
        carbs: "11",
        proteins: "1",
        fat: "0",
        categoryId: 1
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Narandže",
        kcal: "54",
        carbs: "9",
        proteins: "1",
        fat: "0",
        categoryId: 1
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Ribizle (crvene)",
        kcal: "45",
        carbs: "10",
        proteins: "1",
        fat: "0",
        categoryId: 1
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Ribizle (crne)",
        kcal: "63",
        carbs: "14",
        proteins: "1",
        fat: "0",
        categoryId: 1
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Šljive",
        kcal: "58",
        carbs: "14",
        proteins: "1",
        fat: "0",
        categoryId: 1
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Trešnje",
        kcal: "57",
        carbs: "13",
        proteins: "1",
        fat: "0",
        categoryId: 1
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Brokoli",
        kcal: "33",
        carbs: "4",
        proteins: "3",
        fat: "0",
        categoryId: 3
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Cvekla",
        kcal: "37",
        carbs: "8",
        proteins: "2",
        fat: "0",
        categoryId: 3
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Celer",
        kcal: "38",
        carbs: "7",
        proteins: "2",
        fat: "0",
        categoryId: 3
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Pasulj",
        kcal: "110",
        carbs: "21",
        proteins: "7",
        fat: "1",
        categoryId: 3
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Kelj",
        kcal: "46",
        carbs: "5",
        proteins: "4",
        fat: "1",
        categoryId: 3
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Krastavci",
        kcal: "10",
        carbs: "2",
        proteins: "1",
        fat: "0",
        categoryId: 3
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Kupus (kiseli)",
        kcal: "26",
        carbs: "4",
        proteins: "2",
        fat: "0",
        categoryId: 3
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Kupus (slatki)",
        kcal: "52",
        carbs: "7",
        proteins: "4",
        fat: "1",
        categoryId: 3
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Luk",
        kcal: "42",
        carbs: "9",
        proteins: "1",
        fat: "0",
        categoryId: 3
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Šargarepa",
        kcal: "35",
        carbs: "7",
        proteins: "1",
        fat: "0",
        categoryId: 3
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Paprika",
        kcal: "28",
        carbs: "5",
        proteins: "1",
        fat: "0",
        categoryId: 3
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Patlidžan",
        kcal: "26",
        carbs: "5",
        proteins: "1",
        fat: "0",
        categoryId: 3
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Praziluk",
        kcal: "38",
        carbs: "6",
        proteins: "2",
        fat: "0",
        categoryId: 3
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Paradajz",
        kcal: "19",
        carbs: "3",
        proteins: "1",
        fat: "0",
        categoryId: 3
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Tikvica zelena",
        kcal: "17",
        carbs: "3.11",
        proteins: "1.21",
        fat: "0.32",
        categoryId: 3
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Čeri paradajz",
        kcal: "31",
        carbs: "4",
        proteins: "0.9",
        fat: "0",
        categoryId: 3
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Šampinjoni",
        kcal: "24",
        carbs: "3",
        proteins: "3",
        fat: "0",
        categoryId: 3
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Spanać",
        kcal: "23",
        carbs: "2",
        proteins: "2",
        fat: "0",
        categoryId: 3
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Zelena salata",
        kcal: "14",
        carbs: "2",
        proteins: "1",
        fat: "0",
        categoryId: 3
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Zelje",
        kcal: "25",
        carbs: "4",
        proteins: "1",
        fat: "0",
        categoryId: 3
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Jagnjetina",
        kcal: "211",
        carbs: "0",
        proteins: "19",
        fat: "15",
        categoryId: 4
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Jetrena pašteta",
        kcal: "440",
        carbs: "1",
        proteins: "12",
        fat: "40",
        categoryId: 4
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Kobasice",
        kcal: "324",
        carbs: "1",
        proteins: "11",
        fat: "30",
        categoryId: 4
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Konjsko meso",
        kcal: "89",
        carbs: "0",
        proteins: "16",
        fat: "2",
        categoryId: 4
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Mesni narezak",
        kcal: "424",
        carbs: "4",
        proteins: "12",
        fat: "40",
        categoryId: 4
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Mleveno mešano meso",
        kcal: "253",
        carbs: "0",
        proteins: "20",
        fat: "19",
        categoryId: 4
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Ovčetina",
        kcal: "246",
        carbs: "0",
        proteins: "13",
        fat: "24",
        categoryId: 4
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Piletina (belo meso)",
        kcal: "144",
        carbs: "0",
        proteins: "21",
        fat: "3",
        categoryId: 4
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Ćuretina (belo meso)",
        kcal: "231",
        carbs: "0",
        proteins: "22",
        fat: "5",
        categoryId: 4
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Salama parizer",
        kcal: "523",
        carbs: "1",
        proteins: "17",
        fat: "47",
        categoryId: 4
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Salama pileća",
        kcal: "197",
        carbs: "1",
        proteins: "16",
        fat: "14",
        categoryId: 4
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Slanina",
        kcal: "605",
        carbs: "0",
        proteins: "8",
        fat: "60",
        categoryId: 4
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Svinjetina",
        kcal: "345",
        carbs: "0",
        proteins: "18",
        fat: "27",
        categoryId: 4
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Šunka dimljena",
        kcal: "385",
        carbs: "0",
        proteins: "18",
        fat: "33",
        categoryId: 4
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Šunka",
        kcal: "274",
        carbs: "0",
        proteins: "19",
        fat: "20",
        categoryId: 4
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Teletina",
        kcal: "105",
        carbs: "0",
        proteins: "21",
        fat: "3",
        categoryId: 4
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Viršle (svinjske)",
        kcal: "320",
        carbs: "2",
        proteins: "11",
        fat: "29",
        categoryId: 4
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Viršle (pileće)",
        kcal: "258",
        carbs: "7",
        proteins: "13",
        fat: "20",
        categoryId: 4
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Mleko (0,9 % fat)",
        kcal: "40",
        carbs: "4.7",
        proteins: "3.3",
        fat: "0.9",
        categoryId: 6
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Mleko (3,2 % fat)",
        kcal: "66",
        carbs: "4.7",
        proteins: "3.3",
        fat: "3.2",
        categoryId: 6
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Jogurt",
        kcal: "40",
        carbs: "5",
        proteins: "4",
        fat: "4",
        categoryId: 6
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Kiselo mleko",
        kcal: "192",
        carbs: "3",
        proteins: "3",
        fat: "18",
        categoryId: 6
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Pavlaka",
        kcal: "317",
        carbs: "2",
        proteins: "3",
        fat: "32",
        categoryId: 6
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Pavlaka za kuvanje",
        kcal: "206",
        carbs: "4",
        proteins: "2.5",
        fat: "20",
        categoryId: 6
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Puding od čokolade",
        kcal: "134",
        carbs: "21",
        proteins: "3.5",
        fat: "4",
        categoryId: 6
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Sirni namazi (23% fat)",
        kcal: "115",
        carbs: "6",
        proteins: "13",
        fat: "5",
        categoryId: 6
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Topljeni sir (45% fat)",
        kcal: "385",
        carbs: "6",
        proteins: "14",
        fat: "24",
        categoryId: 6
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Tvrdi sir (45% fat)",
        kcal: "372",
        carbs: "3",
        proteins: "25",
        fat: "28",
        categoryId: 6
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Sveži kravlji sir",
        kcal: "72",
        carbs: "4",
        proteins: "15",
        fat: "3",
        categoryId: 6
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Crni hleb",
        kcal: "250",
        carbs: "51",
        proteins: "6",
        fat: "1",
        categoryId: 7
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Dvopek",
        kcal: "397",
        carbs: "77",
        proteins: "10",
        fat: "1",
        categoryId: 7
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Griz",
        kcal: "370",
        carbs: "75",
        proteins: "10",
        fat: "1",
        categoryId: 7
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Kokice",
        kcal: "376",
        carbs: "72",
        proteins: "13",
        fat: "4",
        categoryId: 7
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Kukuruzni hleb",
        kcal: "220",
        carbs: "31",
        proteins: "5",
        fat: "9",
        categoryId: 7
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Kukuruzne pahuljice",
        kcal: "388",
        carbs: "83",
        proteins: "6",
        fat: "1",
        categoryId: 7
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Palenta",
        kcal: "343",
        carbs: "76",
        proteins: "6",
        fat: "0.9",
        categoryId: 7
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Musli",
        kcal: "371",
        carbs: "68",
        proteins: "11",
        fat: "6",
        categoryId: 7
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Polubeli hleb",
        kcal: "252",
        carbs: "52",
        proteins: "3",
        fat: "1",
        categoryId: 7
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Pšenično brašno",
        kcal: "370",
        carbs: "71",
        proteins: "12",
        fat: "2",
        categoryId: 7
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Kukuruzno brašno",
        kcal: "336",
        carbs: "67.24",
        proteins: "9.67",
        fat: "3.2",
        categoryId: 7
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Raženo brašno",
        kcal: "356",
        carbs: "35",
        proteins: "9",
        fat: "1",
        categoryId: 7
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Kokosovo brašno",
        kcal: "641",
        carbs: "6.8",
        proteins: "7.22",
        fat: "60.47",
        categoryId: 7
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Riža ljuštena",
        kcal: "364",
        carbs: "79",
        proteins: "7.32",
        fat: "1",
        categoryId: 7
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Riža neljuštena",
        kcal: "371",
        carbs: "75",
        proteins: "7",
        fat: "2",
        categoryId: 7
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Zobene pahuljice",
        kcal: "402",
        carbs: "66",
        proteins: "14",
        fat: "7",
        categoryId: 7
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Testenina - Makaroni",
        kcal: "332",
        carbs: "71",
        proteins: "9.9",
        fat: "0.5",
        categoryId: 7
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Testenina - Špageti",
        kcal: "340",
        carbs: "75",
        proteins: "11",
        fat: "1.5",
        categoryId: 7
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Grašak",
        kcal: "93",
        carbs: "14",
        proteins: "7",
        fat: "1",
        categoryId: 8
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Boranija",
        kcal: "27",
        carbs: "3.7",
        proteins: "1.9",
        fat: "0.3",
        categoryId: 8
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Kikiriki",
        kcal: "607",
        carbs: "10",
        proteins: "27.8",
        fat: "49.1",
        categoryId: 8
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Kikiriki puter",
        kcal: "651",
        carbs: "9.3",
        proteins: "27",
        fat: "55",
        categoryId: 8
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Soja u zrnu",
        kcal: "427",
        carbs: "26",
        proteins: "38",
        fat: "19",
        categoryId: 8
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Sojin sir (tofu)",
        kcal: "72",
        carbs: "2",
        proteins: "8",
        fat: "4",
        categoryId: 8
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Bakalar",
        kcal: "76",
        carbs: "0",
        proteins: "17",
        fat: "1",
        categoryId: 9
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Dagnja",
        kcal: "66",
        carbs: "2",
        proteins: "12",
        fat: "2",
        categoryId: 9
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Grgeč",
        kcal: "75",
        carbs: "0",
        proteins: "15",
        fat: "2",
        categoryId: 9
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Haringa",
        kcal: "155",
        carbs: "0",
        proteins: "13",
        fat: "10",
        categoryId: 9
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Jastog",
        kcal: "86",
        carbs: "1",
        proteins: "16",
        fat: "2",
        categoryId: 9
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Lignja",
        kcal: "77",
        carbs: "1",
        proteins: "16",
        fat: "1",
        categoryId: 9
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Losos",
        kcal: "217",
        carbs: "0",
        proteins: "20",
        fat: "14",
        categoryId: 9
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Pastrmka",
        kcal: "112",
        carbs: "0",
        proteins: "18",
        fat: "2",
        categoryId: 9
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Sardine u ulju",
        kcal: "240",
        carbs: "1",
        proteins: "24",
        fat: "14",
        categoryId: 9
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Skuša",
        kcal: "195",
        carbs: "0",
        proteins: "19",
        fat: "12",
        categoryId: 9
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Šaran",
        kcal: "65",
        carbs: "0",
        proteins: "10",
        fat: "3",
        categoryId: 9
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Škampi",
        kcal: "91",
        carbs: "0",
        proteins: "17",
        fat: "2",
        categoryId: 9
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Štuka",
        kcal: "85",
        carbs: "0",
        proteins: "17",
        fat: "2",
        categoryId: 9
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Tunjevina u ulju",
        kcal: "303",
        carbs: "0",
        proteins: "24",
        fat: "21",
        categoryId: 9
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Jaje",
        kcal: "167",
        carbs: "1",
        proteins: "13",
        fat: "11",
        categoryId: 5
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Žumance",
        kcal: "377",
        carbs: "0",
        proteins: "16",
        fat: "32",
        categoryId: 5
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Belance",
        kcal: "54",
        carbs: "1",
        proteins: "11",
        fat: "0",
        categoryId: 5
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Maslac",
        kcal: "755",
        carbs: "0",
        proteins: "1",
        fat: "83",
        categoryId: 10
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Margarin",
        kcal: "720",
        carbs: "0",
        proteins: "0",
        fat: "81",
        categoryId: 10
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Majonez",
        kcal: "753",
        carbs: "3",
        proteins: "1",
        fat: "82",
        categoryId: 10
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Majonez light",
        kcal: "341",
        carbs: "6",
        proteins: "1",
        fat: "35",
        categoryId: 10
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Svinjska mast",
        kcal: "900",
        carbs: "0",
        proteins: "0",
        fat: "100",
        categoryId: 10
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Biljna mast",
        kcal: "753",
        carbs: "9",
        proteins: "14",
        fat: "74",
        categoryId: 10
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Tartar sos",
        kcal: "480",
        carbs: "2",
        proteins: "1",
        fat: "52",
        categoryId: 10
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Ulje maslinovo",
        kcal: "900",
        carbs: "0",
        proteins: "0",
        fat: "100",
        categoryId: 10
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Ulje repino",
        kcal: "900",
        carbs: "0",
        proteins: "0",
        fat: "100",
        categoryId: 10
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Ulje od suncokreta",
        kcal: "928",
        carbs: "0",
        proteins: "0",
        fat: "100",
        categoryId: 10
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Čokolada mlečna",
        kcal: "563",
        carbs: "55",
        proteins: "9",
        fat: "33",
        categoryId: 11
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Čokolada za kuvanje",
        kcal: "564",
        carbs: "63",
        proteins: "14",
        fat: "28",
        categoryId: 11
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Čokoladne bombone",
        kcal: "490",
        carbs: "68",
        proteins: "5",
        fat: "22",
        categoryId: 11
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Čips",
        kcal: "540",
        carbs: "50",
        proteins: "7",
        fat: "35",
        categoryId: 11
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Pistaći",
        kcal: "640",
        carbs: "10.5",
        proteins: "22.7",
        fat: "54.8",
        categoryId: 11
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Nutella",
        kcal: "546",
        carbs: "58",
        proteins: "6",
        fat: "31.6",
        categoryId: 11
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Gumene bombone",
        kcal: "345",
        carbs: "88",
        proteins: "0",
        fat: "0",
        categoryId: 11
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Kakao prah",
        kcal: "232",
        carbs: "55",
        proteins: "20",
        fat: "14",
        categoryId: 11
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Marmelada",
        kcal: "261",
        carbs: "66",
        proteins: "0",
        fat: "0",
        categoryId: 11
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Med",
        kcal: "319",
        carbs: "80",
        proteins: "0",
        fat: "0",
        categoryId: 11
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Napolitanke",
        kcal: "550",
        carbs: "62",
        proteins: "4",
        fat: "32",
        categoryId: 11
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Piškote",
        kcal: "393",
        carbs: "70",
        proteins: "12",
        fat: "7",
        categoryId: 11
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Plazma keks",
        kcal: "440",
        carbs: "70",
        proteins: "12",
        fat: "12",
        categoryId: 11
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Puding u prahu",
        kcal: "380",
        carbs: "95",
        proteins: "0",
        fat: "0",
        categoryId: 11
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Šećer kristal",
        kcal: "391",
        carbs: "99.85",
        proteins: "0",
        fat: "0",
        categoryId: 11
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Limunada",
        kcal: "49",
        carbs: "12",
        proteins: "0",
        fat: "0",
        categoryId: 12
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Sok od jabuke",
        kcal: "47",
        carbs: "12",
        proteins: "0",
        fat: "0",
        categoryId: 12
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Sok od narandže",
        kcal: "47",
        carbs: "11",
        proteins: "1",
        fat: "0",
        categoryId: 12
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Sok od grejpfruta",
        kcal: "40",
        carbs: "9",
        proteins: "1",
        fat: "0",
        categoryId: 12
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Sok od ribizle",
        kcal: "50",
        carbs: "12",
        proteins: "0",
        fat: "0",
        categoryId: 12
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Sok od šargarepe",
        kcal: "28",
        carbs: "6",
        proteins: "1",
        fat: "0",
        categoryId: 12
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Sok od grožđa",
        kcal: "71",
        carbs: "18",
        proteins: "0",
        fat: "0",
        categoryId: 12
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Coca Cola",
        kcal: "44",
        carbs: "10.9",
        proteins: "0",
        fat: "0",
        categoryId: 12
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Fanta",
        kcal: "42",
        carbs: "10.2",
        proteins: "0",
        fat: "0",
        categoryId: 12
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Šveps (Schweppes)",
        kcal: "55",
        carbs: "13.3",
        proteins: "0",
        fat: "0",
        categoryId: 12
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Guarana",
        kcal: "47",
        carbs: "11.5",
        proteins: "0",
        fat: "0",
        categoryId: 12
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Svetlo pivo",
        kcal: "45",
        carbs: "4",
        proteins: "1",
        fat: "0",
        categoryId: 12
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Crno vino",
        kcal: "66",
        carbs: "66",
        proteins: "0",
        fat: "0",
        categoryId: 12
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Belo vino",
        kcal: "70",
        carbs: "70",
        proteins: "0",
        fat: "0",
        categoryId: 12
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Rakija",
        kcal: "185",
        carbs: "0",
        proteins: "0",
        fat: "0",
        categoryId: 12
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Šampanjac",
        kcal: "84",
        carbs: "3",
        proteins: "0",
        fat: "0",
        categoryId: 12
      },
      {
        createdAt: new Date(),
        updatedAt: new Date(),
        name: "Viski",
        kcal: "250",
        carbs: "0",
        proteins: "0",
        fat: "0",
        categoryId: 12
      }
    ]);
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("products", null, {});
  }
};
