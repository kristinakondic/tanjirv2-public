"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("users", [
      {
        firstName: "Tina",
        lastName: "Kondić",
        email: "tina@a.a",
        password:
          "$2a$10$jYrpR7Kxa5xJjVcXCV7Xv.JPJGGgPlHhcOq947E8kUr8KNMTJAc..",
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ]);
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("users", null, {});
  }
};
