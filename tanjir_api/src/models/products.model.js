// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require("sequelize");
const DataTypes = Sequelize.DataTypes;

module.exports = function(app) {
  const sequelizeClient = app.get("sequelizeClient");
  const products = sequelizeClient.define(
    "products",
    {
      name: {
        type: DataTypes.STRING,
        allowNull: false
      },
      kcal: {
        type: DataTypes.STRING,
        allowNull: false
      },
      carbs: {
        type: DataTypes.STRING,
        allowNull: false
      },
      proteins: {
        type: DataTypes.STRING,
        allowNull: false
      },
      fat: {
        type: DataTypes.STRING,
        allowNull: false
      }
    },
    {
      hooks: {
        beforeCount(options) {
          options.raw = true;
        }
      }
    }
  );

  // eslint-disable-next-line no-unused-vars
  products.associate = function(models) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
    models.products.belongsTo(models.categories, {
      foreignKey: "categoryId",
      onDelete: "cascade",
      hooks: true
    });
  };

  return products;
};
