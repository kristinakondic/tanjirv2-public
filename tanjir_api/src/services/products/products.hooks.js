const { authenticate } = require("@feathersjs/authentication").hooks;

const includeCategories = async (context) => {
  context.params.sequelize = {
    include: [{ model: context.app.services.categories.Model }],
    raw: false,
  };
};

module.exports = {
  before: {
    all: [],
    find: [includeCategories],
    get: [includeCategories],
    create: [authenticate("jwt")],
    update: [authenticate("jwt")],
    patch: [authenticate("jwt")],
    remove: [authenticate("jwt")],
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
