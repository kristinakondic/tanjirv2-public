import React, { useReducer, createContext } from "react";

const ModalContext = createContext({});

const initialState = {
  visible: false,
  content: null,
  title: ""
};

const reducer = (state: any, action: any) => {
  switch (action.type) {
    case "openModal":
      return {
        ...state,
        visible: true,
        content: action.data,
        title: action.title
      };
    case "closeModal":
      return { ...state, visible: false };
    default:
      return state;
  }
};

const ModalProvider = (props: any) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const value = { state, dispatch };

  return (
    <ModalContext.Provider value={value}>
      {props.children}
    </ModalContext.Provider>
  );
};

const ModalConsumer = ModalContext.Consumer;

export { ModalContext, ModalProvider, ModalConsumer };
