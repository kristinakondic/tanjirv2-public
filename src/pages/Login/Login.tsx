import React, { useState, useEffect } from "react";
import { login } from "../../store/actions/userAction";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { Grid, Header, Form, Image, Segment, Button } from "semantic-ui-react";

const Login = () => {
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");
  const dispatch = useDispatch();
  const history = useHistory();
  const user = useSelector((state: any) => state.user);

  useEffect(() => {
    if (user.token) {
      history.push("/");
    }
  });

  const loginUser = () => {
    dispatch(login(email, password));
  };

  return (
    <>
      <Grid
        textAlign="center"
        style={{ height: "100vh" }}
        verticalAlign="middle"
      >
        <Grid.Column style={{ maxWidth: 450 }}>
          <Header as="h2" color="teal" textAlign="center">
            <Image src="/img/logo.png" /> Prijavite se na Vaš profil
          </Header>
          <Form size="large">
            <Segment stacked>
              <Form.Input
                fluid
                icon="user"
                iconPosition="left"
                placeholder="E-mail adresa"
                onChange={(e) => setEmail(e.target.value)}
              />
              <Form.Input
                fluid
                icon="lock"
                iconPosition="left"
                placeholder="Lozinka"
                type="password"
                onChange={(e) => setPassword(e.target.value)}
              />

              <Button color="teal" fluid size="large" onClick={loginUser}>
                Prijavite se
              </Button>
              {user.error && (
                <p className="login-error">
                  Neispravna kombinacija e-mail adrese i lozinke.
                </p>
              )}
            </Segment>
          </Form>
        </Grid.Column>
      </Grid>
    </>
  );
};

export default Login;
