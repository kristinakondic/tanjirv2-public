import React, { useEffect } from "react";
import FoodList from "../../components/FoodList/FoodList";
import FoodSearch from "../../components/FoodSearch/FoodSearch";
import Plate from "../../components/Plate/Plate";
import PlateResult from "../../components/PlateResult/PlateResult";
import { Responsive } from "semantic-ui-react";
import { useDispatch } from "react-redux";
import { getFood } from "../../store/actions/foodAction";

const Home = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getFood(1, null, ""));
  }, [dispatch]);

  return (
    <div className="home-container">
      <Responsive minWidth={769}>
        <div className="plate-wrapper-container">
          <div className="plate-result-container">
            <PlateResult />
          </div>
          <div className="plate-container">
            <Plate />
          </div>
        </div>
      </Responsive>

      <Responsive maxWidth={768}>
        <PlateResult />
        <Plate smallScreen />
      </Responsive>

      <FoodSearch />
      <FoodList />
    </div>
  );
};

export default Home;
