import React, { FC, ReactElement as RE, useEffect, useState } from "react";
import { Input, Dropdown } from "semantic-ui-react";
import { ReduxStore } from "../../store/types";
import {
  getCategories,
  setCategoryInput,
} from "../../store/actions/categoryAction";
import { setSearchInput } from "../../store/actions/searchAction";
import { setFood, getFood } from "../../store/actions/foodAction";
import { connect } from "react-redux";
import { useDispatch } from "react-redux";

export interface FoodSearchProps {
  food: any;
  setSearchInput: (searchInput: string) => void;
  setCategoryInput: (searchInput: string) => void;
  setFood: (food: any[]) => void;
  getFood: (pageNo: number, categoryId: number | null, searchTerm: any) => any;
  searchInput: string;
  categories: any;
  selectedCategory: any;
}

const FoodSearch: FC<FoodSearchProps> = ({
  food,
  setSearchInput,
  setCategoryInput,
  searchInput,
  categories,
  setFood,
  selectedCategory,
}: FoodSearchProps): RE => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getCategories());
  }, [dispatch]);

  const handleSearch = (search: string) => {
    setSearchInput(search);
    if (search === "") {
      selectedCategory === "Sve kategorije"
        ? dispatch(getFood(1, null, ""))
        : dispatch(getFood(1, selectedCategory, ""));
    } else {
      selectedCategory === "Sve kategorije"
        ? dispatch(getFood(1, null, search))
        : dispatch(getFood(1, selectedCategory, search));
    }
  };

  const handleCategory = (event: any, data: any) => {
    const category = data.value;
    setCategoryInput(category);
    console.log(category);
    category === "Sve kategorije"
      ? dispatch(getFood(1, null, ""))
      : dispatch(getFood(1, category, ""));
    setSearchInput("");
  };

  const CategorySelect = (
    <Dropdown
      className="food-list-category-select"
      button
      basic
      floating
      options={categories}
      onChange={handleCategory}
      defaultValue="Sve kategorije"
    />
  );
  const SearchInput = (
    <Input
      className="food-list-search-input"
      value={searchInput}
      onChange={(e: any) => handleSearch(e.target.value)}
      action={CategorySelect}
      icon="search"
      iconPosition="left"
      placeholder="Pretraži namirnice..."
    />
  );

  return SearchInput;
};

const mapStateToProps = (state: ReduxStore) => ({
  searchInput: state.searchInput,
  categories: state.categories.categories,
  selectedCategory: state.categories.selectedCategory,
  food: state.food.foodList.data,
});

const mapActionsToProps = {
  setSearchInput,
  setCategoryInput,
  getFood,
  setFood,
};

export default connect(mapStateToProps, mapActionsToProps)(FoodSearch);
