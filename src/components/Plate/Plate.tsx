import React, { FC, ReactElement as RE } from "react";
import { connect } from "react-redux";
import { ReduxStore } from "../../store/types";

export interface PlateProps {
  smallScreen?: boolean;
  plate: any;
}

const Plate: FC<PlateProps> = ({ smallScreen, plate }: PlateProps): RE => {
  const plateContent = plate.map((e: any) => (
    <span key={e.name}>
      <span>
        <b>{e.name}</b>{" "}
        <span className="plate-font-size">
          ({e.quantity}g) - {(e.kcal * (e.quantity / 100)).toFixed(0)}kcal
        </span>
      </span>
      <br />
    </span>
  ));

  const renderLargeScreen = (
    <div className="plate-background">
      <span style={{ fontSize: `1px`, color: `white` }}>skrivalica</span>
      <div className="plate-content">{plateContent}</div>
    </div>
  );

  const renderSmallScreen = (
    <div>
      <div className="plate-content-sm">{plateContent}</div>
    </div>
  );

  return (
    <>
      {smallScreen || renderLargeScreen}
      {smallScreen && renderSmallScreen}
    </>
  );
};

const mapStateToProps = (state: ReduxStore) => ({
  plate: state.plate,
});

export default connect(mapStateToProps)(Plate);
