import React, { FC, ReactElement as RE } from "react";

const Footer: FC = (): RE => {
  return (
    <footer className="footer">
      <hr className="divider" />
      <p className="signature"> Tanjir © 2020, Built by Tina</p>
    </footer>
  );
};

export default Footer;
