import React, { FC, ReactElement as RE } from "react";
import { ReduxStore } from "../../store/types";
import { connect } from "react-redux";
import { IPlateResult } from "../../store/actions/plateResultAction";

export interface PlateResultProps {
  plateResult: IPlateResult;
}

const PlateResult: FC<PlateResultProps> = ({ plateResult }): RE => {
  return (
    <span style={{ textAlign: "center" }}>
      <h6 className="plate-result-calories">
        Ukupno kalorija: <b>{plateResult.kcal.toFixed(0)}</b>
      </h6>
      {plateResult.kcal !== 0 && (
        <span>
          <h4 className="plate-result-nutritients">
            Ugljeni hidrati: <b>{plateResult.carbs.toFixed(0)}g</b>{" "}
            <b>
              (
              {(
                100 /
                ((plateResult.carbs + plateResult.proteins + plateResult.fat) /
                  plateResult.carbs)
              ).toFixed(0)}
              %)
            </b>
          </h4>
          <h4 className="plate-result-nutritients">
            Proteini: <b>{plateResult.proteins.toFixed(0)}g</b>{" "}
            <b>
              (
              {(
                100 /
                ((plateResult.carbs + plateResult.proteins + plateResult.fat) /
                  plateResult.proteins)
              ).toFixed(0)}
              %)
            </b>
          </h4>
          <h4 className="plate-result-nutritients">
            Masti: <b>{plateResult.fat.toFixed(0)}g</b>{" "}
            <b>
              (
              {(
                100 /
                ((plateResult.carbs + plateResult.proteins + plateResult.fat) /
                  plateResult.fat)
              ).toFixed(0)}
              %)
            </b>
          </h4>
        </span>
      )}
    </span>
  );
};

const mapStateToProps = (state: ReduxStore) => ({
  plateResult: state.plateResult,
});

export default connect(mapStateToProps)(PlateResult);
