import React, { FC, ReactElement as RE } from "react";
import Header from "../Header/Header";
import Footer from "../Footer/Footer";

const Layout: FC = (props): RE => {
  const { children } = props;
  return (
    <>
      <Header title="TANjiR" />
      <div className="children">{children}</div>
      <Footer />
    </>
  );
};

export default Layout;
