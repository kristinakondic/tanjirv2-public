import React, { FC, ReactElement as RE, useState, useEffect } from "react";
import { Input, Table, Button, Responsive } from "semantic-ui-react";
import { sortBy, map } from "lodash";
import { ReduxStore } from "../../store/types";
import { setPlate } from "../../store/actions/plateAction";
import { setPlateResult } from "../../store/actions/plateResultAction";
import { setFood, getFood } from "../../store/actions/foodAction";
import { connect } from "react-redux";
import { Pagination } from "semantic-ui-react";

export interface FoodListProps {
  food: any;
  total: number;
  setPlate: (plate: any[]) => void;
  setPlateResult: (plate: any[]) => any;
  setFood: (food: any) => void;
  getFood: (pageNo: number, categoryId: number | null, searchTerm: any) => void;
  plate: any[];
}

const FoodList: FC<FoodListProps> = ({
  food,
  setPlate,
  plate,
  setPlateResult,
  setFood,
  getFood,
  total,
}: FoodListProps): RE => {
  const [column, setColumn] = useState(null);
  const [direction, setDirection] = useState<
    "ascending" | "descending" | undefined
  >(undefined);

  const [activePage, setActivePage] = useState(1);

  const handleSort = (clickedColumn: any) => {
    if (column !== clickedColumn) {
      setColumn(clickedColumn);
      setFood(
        sortBy(food, function (obj) {
          return parseInt(obj.clickedColumn, 10);
        })
      );
      setDirection("ascending");
      return;
    }
    console.log(food);
    setFood(food.reverse());
    setDirection(direction === "ascending" ? "descending" : "ascending");
  };

  const addFood = (foodItem: any) => {
    let exists = false;
    let newTanjir = plate.map((f: any) => {
      if (f.name === foodItem.name) {
        exists = true;
        f.quantity = parseFloat(f.quantity) + parseFloat(foodItem.quantity);
      }
      return f;
    });
    exists ? setPlate(newTanjir) : setPlate([...plate, foodItem]);
    exists ? setPlateResult(newTanjir) : setPlateResult([...plate, foodItem]);
  };

  const handleQuantityChange = (name: string, quantity: string) => {
    setFood(
      food.map((e: any) => {
        if (e.name === name) e.quantity = quantity;
        return e;
      })
    );
  };

  const handlePaginationChange = (e: any, { activePage }: any) => {
    setActivePage(activePage);
    console.log(activePage);
    getFood(activePage, null, "");
  };

  return (
    <>
      <Table className="food-list-table" sortable celled fixed>
        <Table.Header className="food-list-table-header">
          <Table.Row>
            <Table.HeaderCell
              sorted={column === "name" ? direction : undefined}
              onClick={() => handleSort("name")}
            >
              {" "}
              Naziv{" "}
            </Table.HeaderCell>
            <Table.HeaderCell
              sorted={column === "kcal" ? direction : undefined}
              onClick={() => handleSort("kcal")}
            >
              {" "}
              Kcal{" "}
            </Table.HeaderCell>
            <Table.HeaderCell
              sorted={column === "carbs" ? direction : undefined}
              onClick={() => handleSort("carbs")}
            >
              {" "}
              UH{" "}
            </Table.HeaderCell>
            <Table.HeaderCell
              sorted={column === "proteins" ? direction : undefined}
              onClick={() => handleSort("proteins")}
            >
              {" "}
              Proteini{" "}
            </Table.HeaderCell>
            <Table.HeaderCell
              sorted={column === "fat" ? direction : undefined}
              onClick={() => handleSort("fat")}
            >
              {" "}
              Masti{" "}
            </Table.HeaderCell>
            <Table.HeaderCell>Kategorija</Table.HeaderCell>
            <Table.HeaderCell>Količina</Table.HeaderCell>
            <Table.HeaderCell></Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {map(
            food,
            ({ name, kcal, carbs, proteins, fat, category, quantity }) => (
              <Table.Row className="food-list-table-row" key={name}>
                <Table.Cell>
                  <Responsive className="food-list-responsive" maxWidth={768}>
                    Naziv:{" "}
                  </Responsive>
                  {name}
                </Table.Cell>
                <Table.Cell>
                  <Responsive className="food-list-responsive" maxWidth={768}>
                    Kcal:{" "}
                  </Responsive>
                  {(kcal * (quantity / 100)).toFixed(0)}
                </Table.Cell>
                <Table.Cell>
                  <Responsive className="food-list-responsive" maxWidth={768}>
                    Ugljeni hidrati:{" "}
                  </Responsive>
                  {(carbs * (quantity / 100)).toFixed(1)}
                </Table.Cell>
                <Table.Cell>
                  <Responsive className="food-list-responsive" maxWidth={768}>
                    Proteini:{" "}
                  </Responsive>
                  {(proteins * (quantity / 100)).toFixed(1)}
                </Table.Cell>
                <Table.Cell>
                  <Responsive className="food-list-responsive" maxWidth={768}>
                    Masti:{" "}
                  </Responsive>
                  {(fat * (quantity / 100)).toFixed(1)}
                </Table.Cell>
                <Table.Cell>{category.name}</Table.Cell>
                <Table.Cell>
                  <Responsive className="food-list-responsive" maxWidth={768}>
                    Količina:{" "}
                  </Responsive>
                  <Input
                    className="food-list-grams-input"
                    value={quantity}
                    type="text"
                    onChange={(e) => handleQuantityChange(name, e.target.value)}
                  />
                </Table.Cell>
                <Table.Cell>
                  <Button
                    basic
                    onClick={() =>
                      addFood({
                        name,
                        kcal,
                        carbs,
                        proteins,
                        fat,
                        category,
                        quantity,
                      })
                    }
                    className="food-list-add-btn"
                  >
                    {" "}
                    Dodaj{" "}
                  </Button>
                </Table.Cell>
              </Table.Row>
            )
          )}
        </Table.Body>
      </Table>
      <div className="pagination">
        <Pagination
          activePage={activePage}
          onPageChange={handlePaginationChange}
          firstItem={null}
          lastItem={null}
          pointing
          secondary
          totalPages={Math.ceil(total / 20)}
        />
      </div>
    </>
  );
};

const mapStateToProps = (state: ReduxStore) => ({
  plate: state.plate,
  searchInput: state.searchInput,
  categories: state.categories.categories,
  selectedCategory: state.categories.selectedCategory,
  food: state.food.foodList.data,
  total: state.food.foodList.total,
});

const mapActionsToProps = {
  setPlate,
  setPlateResult,
  setFood,
  getFood,
};

export default connect(mapStateToProps, mapActionsToProps)(FoodList);
