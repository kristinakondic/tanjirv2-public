import React, { FC, ReactElement as RE } from "react";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import { logout } from "../../store/actions/userAction";
import { Container, Dropdown, Image, Menu } from "semantic-ui-react";

export interface HeaderProps {
  title: string;
}

const Header: FC<HeaderProps> = ({ title }: HeaderProps): RE => {
  const history = useHistory();
  const dispatch = useDispatch();
  const logoutUser = () => {
    localStorage.removeItem("loggedUser");
    localStorage.removeItem("token");
    dispatch(logout());
    history.push("/login");
  };

  return (
    <Menu fixed="top">
      <Container>
        <Menu.Item as="a" header className="_header-title">
          <Image
            size="mini"
            src="/img/logo.png"
            style={{ marginRight: "1.5em" }}
          />
          {title}
        </Menu.Item>
        <Dropdown item simple text="Dropdown">
          <Dropdown.Menu>
            <Dropdown.Item>List Item</Dropdown.Item>
            <Dropdown.Item>List Item</Dropdown.Item>
            <Dropdown.Divider />
            <Dropdown.Header>Header Item</Dropdown.Header>
            <Dropdown.Item>
              <i className="dropdown icon" />
              <span className="text">Submenu</span>
              <Dropdown.Menu>
                <Dropdown.Item>List Item</Dropdown.Item>
                <Dropdown.Item>List Item</Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown.Item>
            <Dropdown.Item>List Item</Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
        <Menu.Item as="a" position="right" onClick={logoutUser}>
          Odjava
        </Menu.Item>
      </Container>
    </Menu>
    /*<header className="_header ">
      <div className="_header-container">
        <h1>
          <Link className="_header-title" to="/">
            {" "}
            {title}{" "}
          </Link>
        </h1>
        <span onClick={logout}>Odjava</span>
      </div>
    </header>*/
  );
};

export default Header;
