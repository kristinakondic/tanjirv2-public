import { Actions } from "../types";
import { SET_SEARCH_INPUT } from "../constants";

export const searchReducer = (state: any = '', action: Actions): any => {

    switch(action.type) {
        case SET_SEARCH_INPUT: 
            return action.payload;
        default: return state;
    }
}