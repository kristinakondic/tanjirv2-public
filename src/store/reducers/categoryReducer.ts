import { Actions } from "../types";
import {
  GET_CATEGORIES_FAIL,
  GET_CATEGORIES_SUCCESS,
  SET_CATEGORY_INPUT,
} from "../constants";

export const initState = {
  categories: [],
  selectedCategory: "Sve kategorije",
  error: false,
  success: false,
};

export const categoryReducer = (state = initState, action: Actions): any => {
  switch (action.type) {
    case SET_CATEGORY_INPUT:
      return {
        ...state,
        selectedCategory: action.payload,
      };
    case GET_CATEGORIES_SUCCESS:
      let categories = action.payload.data.map((c: any) => {
        return { key: c.name, text: c.name, value: c.id };
      });
      categories.push({
        key: "Sve",
        text: "Sve kategorije",
        value: "Sve kategorije",
      });
      return {
        ...state,
        categories,
        success: true,
        error: false,
      };
    case GET_CATEGORIES_FAIL:
      return { ...state, success: false, error: true };
    default:
      return state;
  }
};
