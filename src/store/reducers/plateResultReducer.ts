import { Actions } from "../types";
import { SET_PLATE_RESULT } from "../constants";

export const plateResultReducer = (state: any = {}, action: Actions): any => {

    switch(action.type) {
        case SET_PLATE_RESULT:
            return action.payload;
        default: return state;
    }
}