import { Actions } from "../types";
import { SET_FOOD, GET_FOOD_SUCCESS, GET_FOOD_FAIL } from "../constants";

export const initState = {
  foodList: { total: 0, limit: 0, skip: 0, data: [] },
  error: false,
  success: false,
};

export const foodReducer = (state = initState, action: Actions): any => {
  switch (action.type) {
    case SET_FOOD:
      return {
        ...state,
        foodList: action.payload,
        success: true,
        error: false,
      };
    case GET_FOOD_SUCCESS:
      return {
        ...state,
        foodList: action.payload,
        success: true,
        error: false,
      };
    case GET_FOOD_FAIL:
      return { ...state, success: false, error: true };
    default:
      return state;
  }
};
