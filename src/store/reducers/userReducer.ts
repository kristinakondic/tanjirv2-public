import { User } from "../models/User";
import { Actions } from "../types";
import { USER_LOGIN_SUCCESS, USER_LOGIN_FAIL, USER_LOGOUT } from "../constants";

export const initState = {
  loggedUser: User,
  token: null,
  error: false,
  success: false,
  loggedIn: false,
};

export const userReducer = (state = initState, action: Actions) => {
  switch (action.type) {
    case USER_LOGIN_SUCCESS: {
      return { ...state, token: action.payload, error: false };
    }
    case USER_LOGIN_FAIL: {
      return { ...state, token: null, error: true };
    }
    case USER_LOGOUT: {
      return initState;
    }
    default:
      return state;
  }
};
