import { Actions } from "../types";
import { SET_PLATE } from "../constants";

export const plateReducer = (state: any = [], action: Actions): any => {

    switch(action.type) {
        case SET_PLATE: 
            return action.payload;
        default: return state;
    }
}