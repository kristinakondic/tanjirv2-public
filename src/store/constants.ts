export const SET_PLATE = "plate:setPlate";
export type SET_PLATE = typeof SET_PLATE;

export const SET_PLATE_RESULT = "plateResult:setPlateResult";
export type SET_PLATE_RESULT = typeof SET_PLATE_RESULT;

export const SET_SEARCH_INPUT = "search:setSearchInput";
export type SET_SEARCH_INPUT = typeof SET_SEARCH_INPUT;

export const SET_CATEGORY_INPUT = "category:setCategoryInput";
export type SET_CATEGORY_INPUT = typeof SET_CATEGORY_INPUT;

export const GET_CATEGORIES_SUCCESS = "categories:getCategoriesSuccess";
export type GET_CATEGORIES_SUCCESS = typeof GET_CATEGORIES_SUCCESS;

export const GET_CATEGORIES_FAIL = "categories:getCategoriesFail";
export type GET_CATEGORIES_FAIL = typeof GET_CATEGORIES_FAIL;

export const SET_FOOD = "food:setFood";
export type SET_FOOD = typeof SET_FOOD;

export const GET_FOOD_SUCCESS = "food:getFoodSuccess";
export type GET_FOOD_SUCCESS = typeof GET_FOOD_SUCCESS;

export const GET_FOOD_FAIL = "food:getFoodFail";
export type GET_FOOD_FAIL = typeof GET_FOOD_FAIL;

export const USER_LOGIN_SUCCESS = "user:userLoginSuccess";
export type USER_LOGIN_SUCCESS = typeof USER_LOGIN_SUCCESS;

export const USER_LOGOUT = "user:userLogout";
export type USER_LOGOUT = typeof USER_LOGOUT;

export const USER_LOGIN_FAIL = "user:userLoginFail";
export type USER_LOGIN_FAIL = typeof USER_LOGIN_FAIL;
