export interface IUser {
  firstName: string;
  lastName: string;
  email: string;
}

export const User: IUser = {
  firstName: "",
  lastName: "",
  email: "",
};
