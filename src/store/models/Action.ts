interface IAction {
  type: any;
  payload: any;
}

export const Action = (type: string, data?: any): IAction => ({
  type: type,
  payload: data,
});
