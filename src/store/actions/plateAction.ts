import { SET_PLATE } from '../constants';

export interface ISetPlate {
    type: SET_PLATE;
    payload: any[]
}

export const setPlate = (p: any[]): ISetPlate => ({
    type: SET_PLATE,
    payload: p
});
