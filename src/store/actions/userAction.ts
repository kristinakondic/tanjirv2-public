import axios from "axios";
import { URL } from "../../common/common";
import { Action } from "../models/Action";
import { USER_LOGIN_FAIL, USER_LOGIN_SUCCESS, USER_LOGOUT } from "../constants";
import { ThunkAction, ThunkDispatch } from "redux-thunk";

export interface ILoginUser {
  type: USER_LOGIN_SUCCESS | USER_LOGIN_FAIL;
  payload: any;
}
export interface ILogoutUser {
  type: USER_LOGOUT;
  payload: any;
}
export const login = (
  email: string,
  password: string
): ThunkAction<Promise<void>, {}, {}, ILoginUser> => {
  return (dispatch: ThunkDispatch<{}, {}, ILoginUser>) => {
    return axios
      .post(`${URL}/authentication`, {
        email,
        password,
        strategy: "local",
      })
      .then((response) => {
        if (response.data) {
          localStorage.setItem("loggedUser", response.data.user);
          localStorage.setItem("token", response.data.accessToken);
          dispatch(Action(USER_LOGIN_SUCCESS, response.data.accessToken));
        } else {
          dispatch(Action(USER_LOGIN_FAIL));
        }
      })
      .catch((e) => {
        console.log(e);
        dispatch(Action(USER_LOGIN_FAIL));
      });
  };
};

export const logout = (): ILogoutUser => ({
  type: USER_LOGOUT,
  payload: null,
});
