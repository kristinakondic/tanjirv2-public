import { SET_PLATE_RESULT } from "../constants";

export interface ISetPlateResult {
  type: SET_PLATE_RESULT;
  payload: any;
}

export interface IPlateResult {
  kcal: number;
  carbs: number;
  proteins: number;
  fat: number;
}

export const setPlateResult = (plate: any[]): any => {
  let kcalSum = 0,
    carbsSum = 0,
    protSum = 0,
    mastSum = 0;
  plate.forEach((e: any) => {
    kcalSum += parseFloat(e.kcal) * (e.quantity / 100);
    carbsSum += parseFloat(e.carbs) * (e.quantity / 100);
    protSum += parseFloat(e.proteins) * (e.quantity / 100);
    mastSum += parseFloat(e.fat) * (e.quantity / 100);
  });
  return {
    type: SET_PLATE_RESULT,
    payload: {
      kcal: kcalSum,
      carbs: carbsSum,
      proteins: protSum,
      fat: mastSum,
    },
  };
};
