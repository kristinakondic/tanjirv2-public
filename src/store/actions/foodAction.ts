import { SET_FOOD, GET_FOOD_SUCCESS, GET_FOOD_FAIL } from "../constants";
import axios from "axios";
import { URL } from "../../common/common";
import { Action } from "../models/Action";
import { ThunkAction, ThunkDispatch } from "redux-thunk";

export interface ISetFood {
  type: SET_FOOD;
  payload: any;
}

export const setFood = (p: any): ISetFood => ({
  type: SET_FOOD,
  payload: p,
});

export interface IGetFood {
  type: GET_FOOD_SUCCESS | GET_FOOD_FAIL;
  payload: any;
}

export const getFood = (
  pageNo: number,
  categoryId: number | null,
  searchTerm: any
): ThunkAction<Promise<void>, {}, {}, IGetFood> => {
  return (dispatch: ThunkDispatch<{}, {}, IGetFood>) => {
    const limit = 20;
    console.log(pageNo);
    console.log((pageNo - 1) * limit);
    const skip = (pageNo - 1) * limit;
    return axios
      .get(
        `${URL}/products?$limit=${limit}&$skip=${skip}&${
          categoryId ? "categoryId=" + categoryId : ""
        }&name[$like]=%${searchTerm}%`
      )
      .then((response) => {
        if (response.data) {
          response.data.data = response.data.data.map((e: any) => ({
            ...e,
            quantity: 100,
          }));
          dispatch(Action(GET_FOOD_SUCCESS, response.data));
        } else {
          dispatch(Action(GET_FOOD_FAIL));
        }
      })
      .catch((e) => {
        console.log(e);
        dispatch(Action(GET_FOOD_FAIL));
      });
  };
};
