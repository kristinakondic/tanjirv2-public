import { ThunkAction, ThunkDispatch } from "redux-thunk";
import {
  SET_CATEGORY_INPUT,
  GET_CATEGORIES_FAIL,
  GET_CATEGORIES_SUCCESS,
} from "../constants";
import axios from "axios";
import { URL } from "../../common/common";
import { Action } from "../models/Action";

export interface ISetCategoryInput {
  type: SET_CATEGORY_INPUT;
  payload: string;
}

export const setCategoryInput = (p: any): ISetCategoryInput => ({
  type: SET_CATEGORY_INPUT,
  payload: p,
});

export interface IGetCategories {
  type: GET_CATEGORIES_SUCCESS | GET_CATEGORIES_FAIL;
  payload: any;
}

export const getCategories = (): ThunkAction<
  Promise<void>,
  {},
  {},
  IGetCategories
> => {
  return (dispatch: ThunkDispatch<{}, {}, IGetCategories>) => {
    return axios
      .get(`${URL}/categories`)
      .then((response) => {
        if (response.data) {
          dispatch(Action(GET_CATEGORIES_SUCCESS, response.data));
        } else {
          dispatch(Action(GET_CATEGORIES_FAIL));
        }
      })
      .catch((e) => {
        console.log(e);
        dispatch(Action(GET_CATEGORIES_FAIL));
      });
  };
};
