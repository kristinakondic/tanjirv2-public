import { SET_SEARCH_INPUT } from '../constants';

export interface ISetSearchInput {
    type: SET_SEARCH_INPUT;
    payload: string
}

export const setSearchInput = (p: string): ISetSearchInput => ({
    type: SET_SEARCH_INPUT,
    payload: p
});