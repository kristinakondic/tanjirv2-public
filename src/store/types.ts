import { ISetPlate } from "./actions/plateAction";
import { ISetPlateResult, IPlateResult } from "./actions/plateResultAction";
import { ISetSearchInput } from "./actions/searchAction";
import { IGetCategories, ISetCategoryInput } from "./actions/categoryAction";
import { ISetFood, IGetFood } from "./actions/foodAction";
import { IUser } from "./models/User";
import { ILoginUser, ILogoutUser } from "./actions/userAction";

export interface ReduxStore {
  plate: Array<any>;
  plateResult: IPlateResult;
  searchInput: string;
  categories: any;
  food: any;
  user: IUser;
}

export type Actions =
  | ISetPlate
  | ISetPlateResult
  | ISetSearchInput
  | ISetCategoryInput
  | IGetCategories
  | ISetFood
  | IGetFood
  | ILoginUser
  | ILogoutUser;
