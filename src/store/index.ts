import { createStore, combineReducers, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import { plateReducer } from "./reducers/plateReducer";
import { plateResultReducer } from "./reducers/plateResultReducer";
import { searchReducer } from "./reducers/searchReducer";
import {
  categoryReducer,
  initState as categoriesState,
} from "./reducers/categoryReducer";
import {
  foodReducer,
  initState as foodInitState,
} from "./reducers/foodReducer";
import {
  userReducer,
  initState as userInitState,
} from "./reducers/userReducer";

import thunk from "redux-thunk";

const allReducers = combineReducers({
  plate: plateReducer,
  plateResult: plateResultReducer,
  searchInput: searchReducer,
  categories: categoryReducer,
  food: foodReducer,
  user: userReducer,
});

const initState = {
  plate: [],
  plateResult: { kcal: 0, fat: 0, proteins: 0, carbs: 0 },
  searchInput: "",
  categories: categoriesState,
  food: foodInitState,
  user: userInitState,
};

const store = createStore(
  allReducers,
  initState,
  composeWithDevTools(applyMiddleware(thunk))
);

export default store;
